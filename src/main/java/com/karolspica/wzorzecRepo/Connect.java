package com.karolspica.wzorzecRepo;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;


public class Connect {
	
	
	

	private String url = "jdbc:hsqldb:hsql://localhost/testdb";

	private String createTableActor = "CREATE TABLE ACTOR(id bigint PRIMARY KEY IDENTITY, nazwisko varchar(20), dataUrodzenia date, biografia varchar(400))";
	

	public Connection connection;
	
	public Connect() {
		
		try {
			
			
			connection = DriverManager.getConnection(url);
			Statement statement = connection.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
			
			
			DatabaseMetaData dbmd = connection.getMetaData();
			ResultSet rs = dbmd.getTables(null, null, null, null);
			
			boolean tableActor = sprawdzanieTabel(rs,"ACTOR");
			
			
			if(!tableActor) 
				statement.executeUpdate(createTableActor);
			
				
		}catch(SQLException e ) {
			e.printStackTrace();
		}
		
		
}
	
	public static boolean sprawdzanieTabel(ResultSet rs,String nazwaTabeli ) {
		boolean tableExsist=false;
				try{
		while(rs.next()) {
			if(nazwaTabeli.equalsIgnoreCase(rs.getString("TABLE_NAME"))) {
				tableExsist = true;
				System.out.println("Tabel "+nazwaTabeli+" istnieje");
				break;
			}
		
		}
				rs.beforeFirst();
				}catch (SQLException e) {
					e.printStackTrace();
				}
				return tableExsist;
	}
	public Connection getConnection() {
		return connection;
	}
}
