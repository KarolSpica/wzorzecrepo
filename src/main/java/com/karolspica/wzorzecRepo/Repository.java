package com.karolspica.wzorzecRepo;

import java.util.List;

public interface Repository<T> {
	
	int add (T item);
	void delete();
	List<T> getAll ();
}
