package com.karolspica.wzorzecRepo;

import java.util.Date;
import java.util.List;

import com.karolspica.wzorzecRepo.actor.Actor;
import com.karolspica.wzorzecRepo.actor.ActorRepository;

public class App 
{
    public static void main( String[] args )
    {
    	Date date = new Date(99-02-03);
        Actor karol = new Actor("Karol", date, "dluga biografa" );
        ActorRepository actorRepo = new ActorRepository();
        actorRepo.add(karol);
        List<Actor> aktora = actorRepo.getAll();
        Actor nowy = aktora.get(0);
        System.out.println(nowy);
        actorRepo.delete();
    }
}
