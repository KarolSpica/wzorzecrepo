package com.karolspica.wzorzecRepo.actor;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.karolspica.wzorzecRepo.Connect;
import com.karolspica.wzorzecRepo.Repository;

public class ActorRepository implements Repository<Actor> {

	private PreparedStatement deleteAllActorsStmt;
	private PreparedStatement addActorStmt;
	private PreparedStatement getAllActorsStmt;
	
	Connect connect = new Connect();
		
		public void delete() {
			try {
				deleteAllActorsStmt  = connect.connection.prepareStatement("DELETE FROM ACTOR");
				deleteAllActorsStmt.executeUpdate();
			} catch(SQLException e) {
				e.printStackTrace();
			}
		}
		public int add(Actor actor) {
			java.sql.Date dataSQL = new java.sql.Date(actor.getDateOfBirth().getTime());
			int count = 0;
			try {
				addActorStmt = connect.connection.prepareStatement("INSERT INTO ACTOR (nazwisko, dataUrodzenia, biografia) VALUES(?,?,?)");
				addActorStmt.setString(1, actor.getName());
				addActorStmt.setDate(2, dataSQL);
				addActorStmt.setString(3, actor.getBiography());
				
				count = addActorStmt.executeUpdate();
			} catch(SQLException e) {
				e.printStackTrace();
			}
			return count;
		}
		public List<Actor> getAll(){
			List<Actor> actors = new ArrayList<Actor>();
			
			try {
				getAllActorsStmt = connect.connection.prepareStatement("SELECT nazwisko, dataUrodzenia, biografia FROM ACTOR ");
				ResultSet resultSet = getAllActorsStmt.executeQuery();
				
				while(resultSet.next()) {
					Actor actor = new Actor(resultSet.getString("nazwisko"),
							resultSet.getDate("dataurodzenia"),
							resultSet.getString("biografia"));
					actors.add(actor);
				}
			} catch(SQLException e) {
				e.printStackTrace();
			}
			return actors;
		}
}
